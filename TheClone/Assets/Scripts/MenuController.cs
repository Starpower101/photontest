﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
	[SerializeField] private string VersioName = "0.1";
	[SerializeField] private GameObject UsernameMenu;
	[SerializeField] private GameObject ConnectPanel;

	[SerializeField] private InputField UsernameInput;
	[SerializeField] private InputField CreateGameInput;
	[SerializeField] private InputField JoinGameInput;

	[SerializeField] private GameObject StartButton;

	private void Awake()
	{
		PhotonNetwork.ConnectUsingSettings(VersioName);
	}
    // Start is called before the first frame update
    private void Start()
    {
        UsernameMenu.SetActive(true);
    }

    private void OnConnectedToMaster()
    {
    	PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public void ChangeUsernameInput()
    {
    	if(UsernameInput.text.Length >= 2)
    	{
    		StartButton.SetActive(true);
    	}
    	else{
    		StartButton.SetActive(false);
    	}
    }

    public void SetUsername()
    {
    	UsernameMenu.SetActive(false);
    	PhotonNetwork.playerName = UsernameInput.text;
    }

    public void CreateGame()
    {
    	PhotonNetwork.CreateRoom(CreateGameInput.text, new RoomOptions() { maxPlayers = 4 }, null);
    }

    public void JoinGame()
    {
    	RoomOptions roomOptions = new RoomOptions();	
    	roomOptions.maxPlayers = 4;
    	PhotonNetwork.JoinOrCreateRoom(JoinGameInput.text, roomOptions, TypedLobby.Default);
    }

    public void OnJoinedRoom()
    {
    	PhotonNetwork.LoadLevel("MainRoom");
    }

    // // Update is called once per frame
    // void Update()
    // {
        
    // }
}
