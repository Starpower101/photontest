﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Photon.MonoBehaviour
{
	public PhotonView photonView;
    public Rigidbody2D rb;
    public Animator anim; 
    public GameObject PlayerCamera;
    public SpriteRenderer sr;
    public Text PlayerNameText;

    public bool IsGrounded = false;
    public bool Typing = false;
    public float MoveSpeed;
    public float JumpForce;

    private InputField ChatInputField;

    private void Awake()
    {
    	if(photonView.isMine)
    	{
    		PlayerCamera.SetActive(true);
    		PlayerNameText.text = PhotonNetwork.playerName;
    	}
    	else
    	{
    		PlayerNameText.text = photonView.owner.name;
    		PlayerNameText.color = Color.red;
    	}

        ChatInputField = GameObject.Find("ChatInputField").GetComponent<InputField>();
    }

    private void Update()
    {
        

    	if(photonView.isMine)
    	{
            // if(Typing = false)
            // {
                
            // }
            if(ChatInputField.isFocused)
            {
                Typing = true;
                Debug.Log("Hi");
            }
            else
            {
                Typing = false;
                CheckInput();
            }
    	}
    }

    private void CheckInput()
    {
    	var move = new Vector3(Input.GetAxisRaw("Horizontal"), 0);
    	transform.position += move * MoveSpeed * Time.deltaTime;

    	//move left
    	if(Input.GetKeyDown(KeyCode.A))
    	{
    		photonView.RPC("FlipTrue", PhotonTargets.AllBuffered);	
    	}

    	//move right
    	if(Input.GetKeyDown(KeyCode.D))
    	{
    		photonView.RPC("FlipFalse", PhotonTargets.AllBuffered);	
    	}

    	if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
    	{
    		anim.SetBool("isRunning", true);
    	}
    	else
    	{
    		anim.SetBool("isRunning", false);
    	}
    }

    [PunRPC]
    private void FlipTrue()
    {
    	sr.flipX = true;
    }

    [PunRPC]
    private void FlipFalse()
    {
    	sr.flipX = false;
    }
}
