﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerNew : MonoBehaviour
{
	public GameObject PlayerPrefab;
	public GameObject GameCanvas;
	public GameObject SceneCamera;
	public Text PingText;
	public GameObject disconnectUI;
	private bool Off = false;

	//Player Feed Code
	public GameObject PlayerFeed;
	public GameObject FeedGrid;

	public void Awake()
	{
		GameCanvas.SetActive(true);
	}

	 // Update is called once per frame
    private void Update()
    {
    	CheckInput();
        PingText.text = "Ping: " + PhotonNetwork.GetPing();
    } 

    private void CheckInput()
    {
    	if(Off && Input.GetKeyDown(KeyCode.Escape))
    	{
    		disconnectUI.SetActive(false);
    		Off = false;
    	}
    	else if (!Off && Input.GetKeyDown(KeyCode.Escape))
    	{
    		disconnectUI.SetActive(true);
    		Off = true;
    	}

    }

	public void SpawnPlayer()
	{
		float randomValue = Random.Range (0f, 2f);

		PhotonNetwork.Instantiate(PlayerPrefab.name, new Vector2(this.transform.position.x * randomValue, this.transform.position.y), Quaternion.identity, 0);
		GameCanvas.SetActive(false);
		SceneCamera.SetActive(false);
	}

	public void LeaveRoom()
	{
		PhotonNetwork.LeaveRoom();
		PhotonNetwork.LoadLevel("MainMenu");
	}

	private void OnPhotonPlayerConnected(PhotonPlayer player)
	{
		GameObject obj = Instantiate(PlayerFeed, new Vector2(0, 0), Quaternion.identity);
		obj.transform.SetParent(FeedGrid.transform, false);
		obj.GetComponent<Text>().text = player.name + "joined the room";
		obj.GetComponent<Text>().color = Color.green;
	}

	private void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		GameObject obj = Instantiate(PlayerFeed, new Vector2(0, 0), Quaternion.identity);
		obj.transform.SetParent(FeedGrid.transform, false);
		obj.GetComponent<Text>().text = player.name + "left the room";
		obj.GetComponent<Text>().color = Color.cyan;
	}

    /*// Start is called before the first frame update
    void Start()
    {
        
    }*/

   
}
